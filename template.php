<?php // $Id$

function agencygrass_preprocess_page(&$vars) {
  $vars['ie6'] = FALSE;
  if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.0') !== false)){ $vars['ie6'] = TRUE; }
  $vars['footer_message'] = $vars['footer_message'] . "<div id=\"credits\"><a title=\"Realizzazione Siti Drupal Vicenza\" href=\"http://www.realizzazione-siti-vicenza.com\">R</a><a title=\"Consulente Drupal\" href=\"http://www.siti-drupal.it\">D</a></div>";
}
