<?php // $Id$ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<title><?php print $head_title ?></title>
<?php print $head ?>
<?php print $styles ?>
<?php print $scripts ?>
<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
<?php if ($ie6) { ?>
<!--[if IE 6]>
<style type="text/css">

</style>
<![endif]-->
<?php } ?>
</head>
  <body class="<?php print $body_classes ?>">
  <div id="wrapper">
  <div id="header-wrapper">
    <div id="header" class="fixed">
      <div id="logo">
        <a href="<?php print base_path() ?>" title="<?php print $site_name ?>"><img src="<?php print base_path() . path_to_theme(); ?>/logo.png" alt="<?php print $site_name ?>" title="<?php print $site_name ?>" /></a>
      </div><!--END LOGO -->
      <?php if (isset($secondary_links)){ ?>
      <div id="secondary" ><!--SECONDARY-->
        <?php print theme('links', $secondary_links) ?>
      </div><!--END SECONDARY-->
      <?php } ?>
      <?php if (isset($primary_links)){ ?>
      <div id="primary" ><!--PRIMARY-->
        <?php print theme('links', $primary_links) ?>
      </div><!--END PRIMARY-->
      <?php } ?>
      <?php if($header){?>
      <div id="header-content">
        <?php print $header; ?>
      </div>
      <?php } ?>
    </div>
  </div>
  <div id="page-wrapper">
    <div id="page" class="fixed">
      <div id="main">
        <?php if ($title){?><h1 id="title"><span><?php print $title ?></span></h1><?php } ?>
        <?php if ($messages) { print $messages; } ?>
        <?php if ($help) { print $help; } ?>
        <?php if($tabs){?><div class="tabs"><?php print $tabs ?></div><?php } ?>
        <div id="content"><?php print $content; ?></div>
      </div>
      <?php if($left){?>
      <div id="sidebar">
        <?php print $left; ?>
      </div>
      <?php } ?>
    </div>
  </div>
  <div id="bottom-wrapper">
    <div id="bottom" class="fixed">
      <?php if($bottom_first){ ?>
      <div id="bottom-first">
        <?php print $bottom_first ?>
      </div>
      <?php } ?>
      <?php if($bottom_last){ ?>
      <div id="bottom-last">
        <?php print $bottom_last ?>
      </div>
      <?php } ?>
    </div>
  </div>
  <div id="footer-wrapper">
    <div id="footer" class="fixed">
      <?php print $footer_message ?>
    </div>
  </div>
</div>
<?php print $closure ?>
</body>
</html>